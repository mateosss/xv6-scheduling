# Laboratorio 3 - Planificador MLFQ

## Parte 1

Al principio de la ejecución, como el `scheduler` es Round Robin, cada proceso
encolado se ejecuta por un quanto (número determinado de ticks) y cuando termina
su quanto se hace un trap y el `scheduler` libera al proceso del CPU permitiendo
que cambie la ejecución a otro proceso.

Para hacer que un proceso que duerme sea planificable se hace una llamada a
`wakeup` que setea el estado del proceso a `RUNNABLE`.

El scheduler corre un ciclo, encuentra un proceso con estado RUNNABLE y lo
ejecuta  hasta que: termina su quanto y el trap hace una llamada a `yield` o
hasta que el proceso hace una llamada a una syscall con `sleep()`. Ambas
funciones hacen una llamada a `sched` que devuelve el control al scheduler.
Luego se repite lo anterior.


Las syscall como `read` y `write` (entre otras) tienen una llamada a `sleep` que
setea el estado del proceso a `SLEEPING` haciendo que el scheduler no lo pueda
planificar.

Luego de implementar frutaflops y verduiops y ver como estos se ejecutaban, nos
dimos cuenta que, aunque ambos programas tienen el mismo tiempo para ejecutarse,
frutaflops realiza muchos más cálculos porque solo se usa el CPU. En cambio,
los IOPS son mucho menores por quanto porque necesita usar las syscalls write y
un read que esperan para leer y escribir un archivo.

## Parte 2

Para esta parte implementamos los archivos de cbuffer.c y cbuffer.h para crear
una cola `pqueue` en la cual encolamos los procesos para que sean manejados por
el `scheduler`. Agregamos todas las funciones para el uso de la lista circular
en proc.c.

Cada vez que una función (`userinit`, `fork`, `kill`,`wakeup1` o `yield`) setea
el estado de un proceso a RUNNABLE tenemos también que encolar el proceso a la
cola de prioridades. Como es FIFO, este proceso se encola al final esperando ser
ejecutado.

La función `scheduler`, a la hora de elegir un proceso para ejecutar, hace una
llamada a `cbuffer_read` que se encarga de decolar el primer proceso de la cola
de prioridades.

## Parte 3

Para esto agregamos una nueva propiedad en los procesos denominada priority.
Este valor lo iniciamos en 100000 y a medida que el proceso va gastando su
cuanto este se reduce en 1 (`yield`), a diferencia de que si el proceso libera
el uso del procesador antes de que termine asciende en 1 (`sleep`).

Además, modificamos procdump en proc.c para agregar la caracteristica priority
de los procesos para que se muestre con ctrl+p. Con esta implementacion podemos
comprobar que verduiops se mantiene en prioridad alta mientras que frutaflops va
descendiendo a medida que se ejecuta.

## Parte 4

En esta parte modificamos la `pqueue` para que ahora sea `mlfq`, es decir,
agregamos las 3 colas de prioridad y modificamos el tamaño del cbuffer. El orden
de ejecucion es descendente, es decir, primero el `scheduler` se fija cuales
procesos estan en el nivel mas alto, luego si esta nivel esta vacio, continua
con los de los niveles inferiores.

El nuevo planificador MLFQ asigna a todos los procesos inicialmente el máximo de
prioridad y a medida que van terminando su quanto van descendiendo de prioridad.
De la misma forma, si liberan el procesador antes, suben de prioridad.

Para hacer que un proceso que duerme sea planificable se hace una llamada a
`wakeup` que setea el estado del proceso a `RUNNABLE` y lo encola en la cola de
prioridades, permitiendo que este sea nuevamente planificable.

Cuando se hace una llamada a `scheduler`, este recorre la cola de prioridades y
ejecuta el primer proceso del nivel mas alto con estado RUNNABLE, si este nivel
esta vacío continúa con el suguiente.

Cuando el quanto termina el `scheduler` hace un `yield` que libera al proceso
del CPU permitiendo que cambie la ejecución a otro proceso. Luego, baja la
prioridad y lo vuelve a encolar en la cola de prioridades.

Las syscall como `read` y `write` (entre otras) tienen una llamada a `sleep` que
setea el estado del proceso a `SLEEPING` haciendo que el scheduler no lo pueda
planificar. Luego aumenta su prioridad y espera a ser depertado para planificase
nuevamente.

## En conclusión

![Diagrama de estados de un proceso. Modern Operating System 4ta edición](readme_images/process_state_diagram.png)

Estás son las funciones de xv6 que se encargan de cada una de las transiciones

1. `sleep`
2. `yield`
3. `scheduler`
4. `wakeup1`

Las principales diferencias entre Round Robin y MLFQ es que las transiciones 1,
2 y 4, en MLFQ además de setear `p->state = RUNNABLE` se encola a la `mlfq`. La
segunda gran diferencia es en la transición 3 donde se reemplaza el for del
Round Roubin por un simple `cbuffer_read`

## Gráficos

![Comparación de los planificadores RR y MLFQ respecto a como se degrada un verduiops a medida que agregamos frutaflops](readme_images/MLFQyRR.png)

Comparación de los planificadores RR y MLFQ respecto a como se degrada un
verduiops a medida que agregamos frutaflops

# Extras

## Ticks Más Frecuentes

Para esto le quitamos un cero a `lapic[TICR]`, y agregamos una propiedad
`rticks` que marca los "remaining ticks" o ticks restantes para finalizar el
quanto. En `trap.c` cada vez que hay una interrupción por timer, nos fijamos si
se acabo el quanto del proceso (`p->rticks == 0`) o si existe algún proceso de
mayor prioridad que está `READY` (`should_yield()`). Si el proceso termina su
quanto antes, sleep reinicia `rticks` a 0. Asignamos la cantidad de quantos para
un proceso en el scheduler, como una función lineal de `p->priority`, donde si
es 0, vale `SQUANTUM` (shortest), y si es `MAX_PRIORITY` vale `LQUANTUM`
(longest), y cualquier valor entre medio es una lerp de estos dos.

## HLT

`HLT` es una instrucción de x86 que apaga todo lo que se pueda del procesador
pero que aún pueda recibir interrupciones externas. Está implementado en el
`scheduler`, cuando no encuentra un proceso que correr, el procesador se apaga
hasta la próxima interrupción. Algo a tener en cuenta es que tienen que estar
las interrupciones activadas en el procesador (`sti`) ya que si no, nunca se va
a poder volver de un `hlt`. Se puede observar la mejora viendo como `htop` del
host indica que el procesador emulado no está al 100% (como estaría si el
scheduler no hiciera `hlt`).

## Localidad de Procesos

Se obtuvo agregando la propiedad `cpu` al `struct proc`. También hizo falta
modificar la forma en la que el `scheduler` elige procesos, y ahora `penqueue`
toma el cpu de preferencia como parámetro. Se recorre `mlfq` normalmente, pero
en lugar de hacer un `cbuffer_read` directamente, nos fijamos con la siguiente
condición si realmente deberíamos desencolar el proceso en este cpu:

```c
// Process never run, or run on this cpu, or the original cpu is busy
if (!p->cpu || p->cpu == c || p->cpu->proc != 0) {
  // Corremos el proceso en el cpu actual
} else {
  // Ignoramos el proceso por que puede ser planificado en otro cpu
}
```

Lo que hacemos con esto es aprovechar las cachés de los cpus, y entonces
procesos que ya están en una caché (L1 o L2), van a tender a ser planificados en
el cpu que tiene esa caché.

Para testear esto es un poco más complicado, pero es bueno tener `kvm`
habilitado para hacer emulación por hardware de qemu. Lo podemos testear en la
branch `preserve-cpu-test`, y con `htop` abierto. Hay en esa rama en `scheduler`
dos bloques de código para comentar/descomentar. Corremos dentro de xv6 un único
`frutaflops &` y vemos que el número que sale a la izquierda (que es el puntero
al `struct cpu`) no cambia y lo mismo en htop. Ahora si modificamos `scheduler`
comentando el bloque que preserva la localidad, y descomentando el otro,
observamos que repitiendo el proceso, podemos ver que `frutaflops` va pasando
entre distintos procesadores.

## Tickless Kernel

La idea de un tickless kernel es eliminar el timer que interrumpe el procesador
cientos de veces por segundo. La forma que se nos ocurrió usar el timer como un
recordatorio por procesador de cuando actualizar el tiempo en el sistema. La
forma de hacerlo es tener dos variables `next_event_in` y `next_event_cpu`, la
primera cuenta la cantidad de ticks en los que va a ocurrir un timer interrupt,
y la segundo indica a que procesador le corresponde esa interrupción.

A la hora de recibir un trap de tipo timer, en lugar de hacer `ticks++` hacemos
actualizamos `ticks` y `p->rticks` (ticks restantes para terminar el quanto) de
todos los procesos que estén corriendo. Luego a la hora de schedulear el próximo
proceso seteamos el próximo evento a suceder dentro de la cantidad de `rticks`
del proceso que más rápido está por terminar. La implementación de esto está en
la rama `tickless`.

## Process Tick Counter

Se agregó la propiedad `tcount` al `struct proc`, que cuenta la cantidad de
interrupciones por timer en los que el proceso estaba corriendo. Esta aumenta
en cada `yield`. En lugar de hacer una syscall y un userprogram, resultó más
sensato añadir esta propiedad directamente a `procdump`.

## Starvation

Se puede dar el caso en que un proceso quede en la cola de menor prioridad y
nunca se ejecute porque hay muchos procesos, I/O por ejemplo, que usan menos de
su quanto y luego esperan input. Entre todos pueden llegar a ocupar todo el
tiempo disponible de ejecución y nunca van a bajar de prioridad porque liberan
el CPU antes de terminar, evitando que el proceso menor se ejecute, es decir, se
da `starvation`.

En nuestro caso esto lo podríamos recrear ejecutando muchos verduiops y un solo
frutaflops, con un número suficiente de los primeros nunca se ejecutaría el
último.   
